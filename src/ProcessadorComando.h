/*
 * ProcessaComando.h
 *
 *  Created on: 10 de mar de 2019
 *      Author: NBT
 */

#ifndef SRC_PROCESSADORCOMANDO_H_
#define SRC_PROCESSADORCOMANDO_H_
#include <WString.h>

class ProcessadorComando {
public:
	explicit ProcessadorComando(String comando,char separador=' ');
	String lerProximo();
private:
	String _comando;
	char *_separador;
	char *_resto;
};


#endif /* SRC_PROCESSADORCOMANDO_H_ */
