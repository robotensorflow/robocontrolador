#include <ErriezSerialTerminal.h>
#include <Servo.h>
#include <String.h>
#include <Wire.h>
#include <AFMotor.h>
#include <ProcessadorComando.h>
// LED pin
#define LED_PIN     LED_BUILTIN

void servo1(String args);
void servo2(String args);
void motorFrente(int n);
void motorTras(int n);
void motorParar(int n);
void motorFreiar(int n);
void motorVelocidade(int n);

#define SERVO1_PWM 10
#define SERVO2_PWM 9

#define FORWARD 1
#define BACKWARD 2
#define BRAKE 3
#define RELEASE 4

AF_DCMotor motor1(1);
AF_DCMotor motor2(2);
AF_DCMotor motor3(3);
AF_DCMotor motor4(4);

Servo servo_1;
Servo servo_2;

void recebido(int howMany);

void setup() {
	Wire.begin(0x18);
	Wire.onReceive(recebido);
	motor1.setSpeed(255);
	motor2.setSpeed(255);
	motor3.setSpeed(255);
	motor4.setSpeed(255);

	servo_1.attach(SERVO1_PWM);
	servo_2.attach(SERVO2_PWM);
    servo_1.write(25);
	servo_2.write(0);
}

void processaComando(String comando) {

	ProcessadorComando leitor(comando,' ');
	String cmd = leitor.lerProximo();
	if(cmd.equals("servo1")){
		String args = leitor.lerProximo();
		if(args){
			servo1(args);
		}
	}else if(cmd.equals("servo2")){
		String args = leitor.lerProximo();
		if(args){
			servo2(args);
		}
	}else if(cmd.equals("frente")){
		String args = leitor.lerProximo();
		if(args){

			motorFrente(atoi(args.c_str()));
		}
	}else if(cmd.equals("parar")){
		String args = leitor.lerProximo();
		if(args){
			motorParar(atoi(args.c_str()));
		}
	}else if(cmd.equals("freiar")){
		String args = leitor.lerProximo();
		if(args){
			motorFreiar(atoi(args.c_str()));
		}
	}else if(cmd.equals("tras")){
		String args = leitor.lerProximo();
		if(args){
			motorTras(atoi(args.c_str()));
		}
	}else if(cmd.equals("velocidade")){
		String args = leitor.lerProximo();
		if(args){
			motorVelocidade(atoi(args.c_str()));
		}
	}
}

void recebido(int howMany) {
	String str;
	Wire.read();
	while (Wire.available()) {
		char c = Wire.read();
		str.concat(c);
	}
	str.trim();
	return processaComando(str);
}

void loop() {

}
void motorVelocidade(int n){
	motor1.setSpeed(n);
	motor2.setSpeed(n);
	motor3.setSpeed(n);
	motor4.setSpeed(n);
}
void motorFrente(int n){
	if(n == 1){
		motor1.run(FORWARD);
	}else if(n == 2){
		motor2.run(FORWARD);
	}else if(n == 3){
		motor3.run(FORWARD);
	}else if(n == 4){
		motor4.run(FORWARD);
	}
}
void motorFreiar(int n){
	if(n == 1){
		motor1.run(BRAKE);
	}else if(n == 2){
		motor2.run(BRAKE);
	}else if(n == 3){
		motor3.run(BRAKE);
	}else if(n == 4){
		motor4.run(BRAKE);
	}
}
void motorParar(int n){
	if(n == 1){
		motor1.run(RELEASE);
	}else if(n == 2){
		motor2.run(RELEASE);
	}else if(n == 3){
		motor3.run(RELEASE);
	}else if(n == 4){
		motor4.run(RELEASE);
	}
}
void motorTras(int n){
	if(n == 1){
		motor1.run(BACKWARD);
	}else if(n == 2){
		motor2.run(BACKWARD);
	}else if(n == 3){
		motor3.run(BACKWARD);
	}else if(n == 4){
		motor4.run(BACKWARD);
	}
}
void servo1(String args)
{
    int val = atoi(args.c_str());
    if(val<25)
    	return;
    if(val>200)
    	return;
    servo_1.write(val);
}
void servo2(String args)
{
    int val = atoi(args.c_str());
    if(val<0)
    	return;
    if(val>90)
    	return;
    servo_2.write(val);
}
