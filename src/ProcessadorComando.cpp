/*
 * ProcessaComando.cpp
 *
 *  Created on: 10 de mar de 2019
 *      Author: NBT
 */

#include <ProcessadorComando.h>

ProcessadorComando::ProcessadorComando(String comando,char separador) {
	_comando = comando;
	_resto = _comando.c_str();
	_separador = separador;

}
String ProcessadorComando::lerProximo(){
	char* cmd;
    cmd = strtok_r(_resto, " ", &_resto);
	return cmd;
}


